const { Books } = require('../models')

exports.landingBook =  (async(req,res,next)=>{
  try{
    const foundBook = await Books.findAll()
    res.status(200).json({ 
      status: true, 
      message: `These are lists of authors`,
      data : {foundBook}
  })
  }
  catch (err) {
    res.status(404).json({err})
  }
})

exports.createBook = (async(req,res,next)=>{
  try {
    const createdBook = await Books.create({
      title: req.body.title,
      page: req.body.page,
      price: req.body.price,
      author_id: req.body.author_id
    })
    res.status(201).json({
      status:true,
      message:`Successfully created book with data :`,
      data: { createdBook }
    })
  } catch (err){
    res.status(406).json(err)
  }
  })
  

exports.updateBook = (async(req,res,next)=>{
  try {
    const foundBook = await Books.findByPk(req.params.id);
    if(foundBook){
      const updatedBook = await foundBook.update({
        title: req.body.title,
        page: req.body.page,
        price: req.body.price,
        author_id: req.body.author_id
      }, {
        where: {
          id: req.params.id
        }
      });
      res.status(202).json({
        status: true,
        message: `Successfully update book with id: ${req.params.id}`,
        updated: {updatedBook}
      })

    } else {
      res.status(400).json({
        status: false,
        message: `Can not found book with id: ${req.params.id}`,
      })
    }
    
  } 
  catch (err) {
    res.json(err)
  };
});
    
exports.deleteBook =(async (req, res) => {
  try{
    const foundBook = await Books.findByPk(req.params.id);
    if(foundBook){
      const deletedBook = await Books.destroy({
        where: {
            id: req.params.id
        }
      })
      res.status(202).json({
        status: true,
        message: `Successfully deleted book with ID ${req.params.id}`,
        deleted: {deletedBook}
      })
    } else {
      res.status(400).json({
        status: false,
        message: `Can not found book with ID ${req.params.id}`,
      })
    }
  }
  catch (err){
    res.json(err)
  }
  
});