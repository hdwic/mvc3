const { Authors, Books } = require('../models')

// exports.landingPage = ((req,res,next)=>{
//     let findauthor;
//     Authors.findAll()
//     .then(author => {
//           findauthor = author;
            
//       })
//     // res.status(200).json(findauthor)
//     .catch(err =>{
//       console.log(err)
//     })
//     res.render('index', { title: 'Landing Page', display:'Home Page of', value: findauthor});
//     // res.status(202).json({findauthor})
//   })

exports.landingAuthor =  (async(req,res,next)=>{
  try {
    const foundAuthor = await Authors.findAll()
    res.status(200).json({
        status: true,
        message: 'Author Lists!',
        data: { foundAuthor }
    }) 
    }
    catch (err) {
        res.status(404).json({
            status: false,
            data : {err}
        })
    }
})

exports.createAuthor = (async(req,res,next)=>{
    
    try{ 
        const cekEmail = await Authors.findOne({ where: { email: req.body.email } });
        if (cekEmail === null){
            const createdAuthor = await Authors.create({
                name: req.body.name,
                email: req.body.email,
            })
            res.status(201).json({
                status: true,
                message: 'Author Created!',
                data: { createdAuthor }
            }) 
    
        } else {
            res.status(406).json({
                status: false,
                message: `${req.body.email} already exist, use other email`,
            })
        }
    
    } 
    catch (err) {
        res.json(err)
    }
})

exports.updateAuthor = (async(req,res,next)=>{
    try {
      const foundAuthor = await Authors.findByPk(req.params.id);
    //   console.log(foundAuthor)
      if(foundAuthor){
        const updatedAuthor = await Authors.update({
            name : req.body.name,
            email: req.body.email,
          }, {
            where: {
              id: req.params.id
            }
          });
          res.status(202).json({
              status: true,
              message: `user with ID ${req.params.id} successfuly updated`,
              updated: {foundAuthor}
          })
      } else {
        res.status(400).json({
            status: false,
            message: `user with ID ${req.params.id} not found`
        })
      }
    }
    catch (err) {
        res.json(err)
    }
});

exports.deleteAuthor =(async (req, res) => {
    try {
        const deletedAuthor = await Authors.destroy( { where: { id: req.params.id} } )
        res.status(202).json({
            status: true,
            message: `user with ID ${req.params.id} successfuly deleted`,
            updated: {deletedAuthor}
        })
    } catch (err) {
        res.json(err)
      };
})

exports.showAuthor = (async(req,res) =>{
    const showAuthor = await Authors.findAll({
        where: {id: req.params.id}, include : 'books'
    })
    // console.log(`cek showAuthor = ${showAuthor}`)
    if(showAuthor[0]){
        res.status(200).json({
            status: true,
            message: `Showing Books by ${showAuthor[0].name}`,
            data:showAuthor})
    } else {
        res.status(400).json({
            status: false,
            message: `Author with ID ${req.params.id} not found`
        })
    }
    
})

