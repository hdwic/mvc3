var express = require('express');
var router = express.Router();


//using controller for index
const indexController = require('../controllers/BookController.js')

router.get('/books', indexController.landingBook);
router.post('/books',indexController.createBook);
router.put('/books/:id', indexController.updateBook);
router.delete('/books/:id', indexController.deleteBook);

module.exports = router;