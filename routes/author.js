var express = require('express');
var router = express.Router();


//using controller for index
const indexController = require('../controllers/AuthorController.js')


router.get('/authors', indexController.landingAuthor);
router.post('/authors',indexController.createAuthor);
router.put('/authors/:id', indexController.updateAuthor);
router.delete('/authors/:id', indexController.deleteAuthor);
router.get('/authors/:id',indexController.showAuthor)


module.exports = router;